FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env
WORKDIR /App


# ENV ENVIRONMENT=ENVIRONMENT
# ENV NEXT_PUBLIC_SUPABASE_URL=NEXT_PUBLIC_SUPABASE_URL
# ENV SUPABASE_SERVICE_ROLE_KEY=SUPABASE_SERVICE_ROLE_KEY
# ENV CLOUDMANAGER_RESEND_API_TOKEN=CLOUDMANAGER_RESEND_API_TOKEN
# ENV STRIPE_SECRET_KEY=STRIPE_SECRET_KEY
# ENV NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY=NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY

# Copy everything
COPY *.csproj .
# Restore as distinct layers
RUN dotnet restore
COPY . ./
# Build and publish a release
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /App
COPY --from=build-env /App/out .

ENTRYPOINT ["dotnet", "usage-monitor.dll"]
