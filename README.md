# Dex usage monitor

## How to build

```bash
# build it 
docker buildx build --platform linux/amd64 -t usage-monitor --tag usage-monitor:x86 .

# then tag it:
docker tag usage-monitor:0.0.1 codeberg.org/dex/usage-monitor:0.0.1

# then push it
docker push codeberg.org/dex/usage-monitor:0.0.1
```
