﻿using System.Text.Json;

/*
  how to run
 ENVIRONMENT=PRODUCTION infisical run --env=dev --path=/supabase  -- dotnet run                                                                                                            23:34:52
*/

internal class Program
{
    private static async Task Main(string[] args)
    {
        // let's first just initialize all the variables that we need
        var url =
            Environment.GetEnvironmentVariable("NEXT_PUBLIC_SUPABASE_URL")
            ?? throw new Exception("supabase url is null and cannot be");

        var key =
            Environment.GetEnvironmentVariable("SUPABASE_SERVICE_ROLE_KEY")
            ?? throw new Exception("supabase key is null and cannot be");
        var env =
            Environment.GetEnvironmentVariable("ENVIRONMENT")
            ?? throw new Exception("ENVIRONMENT is null and cannot be");

        var stripeSecretKey =
            Environment.GetEnvironmentVariable("STRIPE_SECRET_KEY")
            ?? throw new Exception("stripe secret key is null and cannot be");

        // every hour just see if we need to report the usage
        while (true)
        {
            try
            {
                var stripeMonitor = new StripeMonitor(url, key, env, stripeSecretKey);
                await stripeMonitor.UpdateAllUsage();
                Thread.Sleep(TimeSpan.FromHours(1));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
