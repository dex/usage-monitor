using System.Text.Json;
using k8s;
using Stripe;

/*
 * this class is just responsible for monitoring the usage of dex users and reporting the data to stripe
 * the only outside function that should be called is UpdateAllUsage
 */
public class StripeMonitor
{
    // Prometheus Variables
    private static readonly string promQueryStringStart =
        "sum(avg_over_time(node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate{cluster=\"\",namespace=\"user-";
    private static readonly string promQueryStringEnd = "\"}[1d]))";
    private static readonly string promService = "prometheus-operated";
    private static readonly string promUrl = $"http://{promService}:9090/api/v1/query?query=";
    private readonly HttpClient client;

    // Supabase Variables
    private readonly string url;
    private readonly string key;
    private readonly string env;
    private Supabase.Client? supabase; // the authorized object that we use to do everything

    // Stripe Variables
    private readonly string stripeSecretKey;

    public StripeMonitor(
        string supabaseUrl,
        string supabaseKey,
        string environment,
        string stripeKey
    )
    {
        client = new();
        url = supabaseUrl;
        key = supabaseKey;
        env = environment;
        stripeSecretKey = stripeKey;
    }

    private static string MakeQueryString(string subdomain)
    {
        var queryString =
            promUrl + Uri.EscapeDataString(promQueryStringStart + subdomain + promQueryStringEnd);
        return queryString;
    }

    // just gets and returns the float user amount from the prometheus service
    private async Task<double?> GetUsageAmount(string subdomain)
    {
        var queryString = MakeQueryString(subdomain);
        var res = await client.GetAsync(queryString);
        string? data = res.Content.ReadAsStringAsync().Result;
        Console.WriteLine("data from request: ", data);
        var usageDataResponse = JsonSerializer.Deserialize<UsageDataResponse>(data ?? "");
        return usageDataResponse switch
        {
            null => null,
            _ => parseUsageData(usageDataResponse)
        };

        static double? parseUsageData(UsageDataResponse data)
        {
            try
            {
                var value = data.data?.result?[0]["value"].ToString();
                if (value == null)
                {
                    return null;
                }
                var valueList = JsonDocument.Parse(value);
                if (valueList.RootElement.ValueKind != JsonValueKind.Array)
                {
                    return null;
                }
                var root = valueList.RootElement;
                return root.ValueKind switch
                {
                    JsonValueKind.Array => double.Parse(root[1].GetString() ?? ""),
                    _ => null
                };
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return null;
            }
        }
    }

    private async Task ConnectToSupabaseAsync()
    {
        // * try in a loop to connect to the supabase client
        while (true)
        {
            try
            {
                var options = new Supabase.SupabaseOptions { AutoConnectRealtime = true };
                supabase = new Supabase.Client(url, key, options);
                await supabase.InitializeAsync();
                Console.WriteLine("Connected to supabase db");
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex}");
                Console.WriteLine($"ERROR connecting to supabase, trying again in 5 seconds");
                Thread.Sleep(5_000);
            }
        }
    }

    // just queries the supabase db for the customer id of the given subdomain
    // NOTE: very unefficient rn, need to change later
    private async Task<string?> GetCustomerId(string subdomain)
    {
        try
        {
            while (supabase == null)
            {
                await ConnectToSupabaseAsync();
            }

            var userSubdomain = await supabase
                .From<PrivateCloud>()
                .Select("id")
                .Filter("subdomain", Postgrest.Constants.Operator.Equals, subdomain)
                .Get();

            var userSubdomainId = userSubdomain.Models[0].Id;

            var userPrivateCloud = await supabase
                .From<UserPrivateCloud>()
                .Select("*")
                .Filter("private_cloud_id", Postgrest.Constants.Operator.Equals, userSubdomainId)
                .Get();

            foreach (var model in userPrivateCloud.Models)
            {
                return model.CustomerId;
            }
            return null;
        }
        catch (Exception e)
        {
            Console.WriteLine("Error getting customer id: ", subdomain);
            Console.WriteLine(e);
            return null;
        }
    }

    private string? GetSubscriptionItemId(string customerId)
    {
        try
        {
            var options = new SubscriptionListOptions { Limit = 1, Customer = customerId, };
            var service = new SubscriptionService();
            StripeList<Subscription> subscriptions = service.List(options);
            for (int i = 0; i < subscriptions.Data.Count; i++)
            {
                var subscription = subscriptions.Data[i];
                if (subscription.Status is "active" or "trialing")
                {
                    for (int j = 0; j < subscription.Items.Data.Count; j++)
                    {
                        var item = subscription.Items.Data[j];
                        if (item.Price.Active == true)
                        {
                            return item.Id;
                        }
                    }
                }
            }
            return null;
        }
        catch (Exception e)
        {
            Console.WriteLine("Error getting subscription item id: ", customerId);
            Console.WriteLine(e);
            return null;
        }
    }

    // this function just sends the usage amount to Stripe for the given user
    private async Task resportUsage(string subdomain)
    {
        try
        {
            var usageAmount = await GetUsageAmount(subdomain);
            Console.WriteLine($"Usage amount: {usageAmount}");
            int usageAmountTotal = (int)(usageAmount * 60 * 60 * 24); // convert to seconds
            Console.WriteLine($"Usage amount total: {usageAmountTotal}");

            var customerId = await GetCustomerId(subdomain);
            if (customerId == null)
            {
                Console.WriteLine("Error getting customer id for subdomain: ", subdomain);
                return;
            }

            // now let's get the customer object
            StripeConfiguration.ApiKey = stripeSecretKey;
            var service = new CustomerService();
            var customer = service.Get(customerId);

            // now let's get the subscription id
            var subscriptionId = GetSubscriptionItemId(customerId);

            var options = new UsageRecordCreateOptions { Quantity = usageAmountTotal, };
            var usageService = new UsageRecordService();
            var usageRecord = usageService.Create(subscriptionId, options);
        }
        catch (Exception e)
        {
            Console.WriteLine("Error reporting usage for subdomain: ", subdomain);
            Console.WriteLine(e);
        }
    }

    // just gets and returns all user namespaces in their full form (so "user-lucas" instead of "lucas)
    private List<string> getAllUserNamespaces()
    {
        try
        {
            var config = KubernetesClientConfiguration.InClusterConfig();
            var client = new Kubernetes(config);
            var namespaces = client.CoreV1.ListNamespace();
            var userNamespaces = new List<string>();
            Console.WriteLine("Listing all user namespaces:");
            foreach (var ns in namespaces.Items)
            {
                var nsName = ns.Metadata.Name;
                if (nsName.StartsWith("user-"))
                {
                    userNamespaces.Add(nsName);
                    Console.WriteLine(nsName);
                }
            }
            Console.WriteLine("Done");

            return userNamespaces;
        }
        catch (Exception e)
        {
            Console.WriteLine("Error getting all user namespaces");
            Console.WriteLine(e);
            return new List<string>();
        }
    }

    private bool IsTimeToRecordUsage()
    {
        // rn we're just checking if the time is between 12am and 1am, should prolly make this more sophisticated later
        // Get the current time
        DateTime currentTime = DateTime.Now;

        // Create DateTime objects representing 12:00 AM and 1:00 AM
        DateTime midnight = currentTime.Date; // Set time to midnight (00:00:00)
        DateTime oneAM = currentTime.Date.AddHours(1); // Set time to 1:00 AM

        // Check if the current time is between 12:00 AM and 1:00 AM
        if (currentTime >= midnight && currentTime < oneAM)
        {
            Console.WriteLine("The current time is between 12:00 AM and 1:00 AM.");
            return true;
        }
        else
        {
            Console.WriteLine("The current time is not between 12:00 AM and 1:00 AM.");
            return false;
        }
    }

    public async Task UpdateAllUsage()
    {
        if (!IsTimeToRecordUsage())
        {
            return;
        }
        // get all the subdomains
        var subdomains = getAllUserNamespaces();
        foreach (var subdomain in subdomains)
        {
            // get rid of the "user-" part
            // just get the actual subdomain
            var justSubdomain = subdomain.Substring(5);
            await resportUsage(justSubdomain);
        }
    }
}

internal class UsageDataResponse
{
    public string? status { get; set; }
    public UsageData? data { get; set; }
}

internal class UsageData
{
    public string? resultType { get; set; }
    public List<Dictionary<string, object>>? result { get; set; }
}

internal class ResultValue { }
