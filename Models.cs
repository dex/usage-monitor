using Postgrest.Attributes;
using Postgrest.Models;

// Given the following Model (City.cs)
[Table("user_private_cloud")]
class UserPrivateCloud : BaseModel
{
    /* [Reference(typeof(PrivateCloud))] */
    [Column("private_cloud_id")]
    public int PrivateCloudId { get; set; }

    [Column("customer_id")]
    public string? CustomerId { get; set; }
}

[Table("private_clouds")]
class PrivateCloud : BaseModel
{
    [PrimaryKey("id", false)]
    public int Id { get; set; }

    [Column("subdomain")]
    public string Subdomain { get; set; }
}
